'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('time_ranges', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      time_range: {
          type: Sequelize.RANGE(Sequelize.DATE),
          allowNull: false,
          defaultValue: []
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('time_ranges');
  }
};