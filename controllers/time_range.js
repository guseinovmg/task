'use strict';

const db = require('../models/index');

function create(start, end) {
    return new Promise((resolve, reject) => {
        db.time_ranges.find({
            where: {
                time_range: {
                    $overlap: [start, end]
                }
            },
            limit: 1
        }).then(result => {
            if (result !== null) {
                return Promise.reject({overlap: true, message: `There are periods that overlaps ${start} - ${end}`});
            } else {
                return db.time_ranges.create({
                    time_range: [start, end]
                })
            }
        }).then(result => {
            log('create res ', result.dataValues);
            resolve(result.dataValues);
        }).catch(error => {
            log('create err ', error);
            reject(error);
        });
    });
}

function get(start, end) {
    return new Promise((resolve, reject) => {
        db.time_ranges.findAll({
            where: {
                time_range: {
                    $overlap: [start, end]
                }
            },
            raw: true
        }).then(result => {
            log('get res ', result);
            resolve(result);
        }).catch(error => {
            log('get err ', error);
            reject(error);
        });
    });
}

function remove(id) {
    return new Promise((resolve, reject) => {
        db.time_ranges.destroy({
            where: {id: id}
        }).then(result => {
            log('remove res ', result);
            resolve(result);
        }).catch(error => {
            log('remove err ', error);
            reject(error);
        });
    });
}

module.exports = {create, get, remove};