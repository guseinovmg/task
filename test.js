'use strict';

const should = require('should');

const axios = require('axios');

const URL = 'http://localhost:3333/time_range';

describe('Time range', function () {
    let id = -1;
    describe('Create', function () {
        it('should return created time range', function () {
            return axios.post(
                URL,
                {
                    start: '2016-01-01 00:00:00+00:00',
                    end: "2016-01-03 00:00:00+00:00"
                }
            ).then((response) => {
                console.log('Response:', response.data);
                response.data.should.have.property('id').which.is.a.Number();
                id = response.data.id;
            });
        });
    });

    describe('Read', function () {
        it('should return time ranges', function () {
            return axios({
                method: 'GET',
                url: URL,
                params: {
                    start: "2016-01-01 00:00:00+00:00",
                    end: "2016-01-07 00:00:00+00:00"
                }
            }).then((response) => {
                console.log('Response:', response.data);
                response.data.should.be.Array();
                response.data.forEach(range => {
                    range.should.have.property('id').which.is.a.Number();
                    range.time_range.should.be.instanceof(Array).and.have.lengthOf(2);
                })
            })
        });
    });

    describe('Delete', function () {
        it('should return {deleted:1} or {deleted:0}', function () {
            return axios.delete(
                URL + '/' + id
            ).then((response) => {
                console.log('Response:', response.data);
                response.data.should.have.property('deleted').which.is.a.Number();
            })
        });
    });
});