# README #

### Установка

Версия Node.js 8.9.0, Postgres 10,  Ubuntu 16.04

Перед работой надо создать в БД в Postgres, имя пользователя, пароль и имя БД установить в файле db.js.

`git clone git@bitbucket.org:guseinovmg/task.git`

`cd task`

`npm install`

`node_modules/.bin/sequelize db:migrate`

`sudo npm install mocha -g`  (для запуска тестов)


### Запуск

`node index.js`

### Тест

`mocha test.js`

Примечание : Тесты выполняют операции CRUD, поэтому  в БД не остаются записи, т.к. последняя операция - удаление записи.

