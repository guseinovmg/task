'use strict';
module.exports = (sequelize, DataTypes) => {
    var time_ranges = sequelize.define('time_ranges', {
        time_range: {
            type: DataTypes.RANGE(DataTypes.DATE),
            allowNull: false,
            defaultValue: []
        }
    }, {
        timestamps: false,
        classMethods: {
            associate: function (models) {
                // associations can be defined here
            }
        }
    });
    return time_ranges;
};