"use strict";

const express = require("express");

global.log = console.log;

const app = express();

const body_parser = require('body-parser');

app.use(body_parser.json());

app.use('/time_range', require('./routes/time_range.js'));

app.listen(3333, function () {
    console.log('App listening on port 3333!');
});