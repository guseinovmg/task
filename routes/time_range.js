'use strict';

const express = require('express');

const router = express.Router();

const tr_controller = require('../controllers/time_range');

router.post('/', (request, response, next) => {
    log('router post', request.body);
    tr_controller.create(request.body.start, request.body.end)
        .then(result => {
            response.send(result);
        })
        .catch(error => {
            if (error.overlap) {
                response.status(406).send(error.message);
            } else {
                next(error)
            }
        });
});

router.get('/', (request, response, next) => {
    log('router get', request.query);
    tr_controller.get(request.query.start, request.query.end)
        .then(result => {
            response.send(result);
        })
        .catch(error => {
            next(error);
        });
});

router.delete('/:id', (request, response, next) => {
    log('router delete', request.params);
    tr_controller.remove(request.params.id)
        .then(result => {
            response.send({deleted: result});
        })
        .catch(error => {
            next(error);
        });
});

module.exports = router;